# Changelog

## Version 3.1.0

- added the status in the identification object
- added the identification object to the ResultDTO class


## Version 3.0.0

- **Namespace for the Postgres configuration file changed**
- added support for catalogs
- added the maven site documentation
- added the REST interface documentation in API Blueprint
