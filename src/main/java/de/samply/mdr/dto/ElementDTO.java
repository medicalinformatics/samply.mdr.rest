/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dto;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ElementDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The identification used to get this element. Useful if you get the latest dataelement
     * and want to know the real URN.
     */
    private IdentificationDTO identification;

    /**
     * The slots for this element
     */
    private Collection<SlotDTO> slots;

    /**
     * A list of definitions, usually this list has at least one item.
     */
    private Collection<DefinitionDTO> designations;

    /**
     * @return the identification
     */
    public IdentificationDTO getIdentification() {
        return identification;
    }

    /**
     * @param identification the identification to set
     */
    public void setIdentification(IdentificationDTO identification) {
        this.identification = identification;
    }

    /**
     * @return the slots
     */
    public Collection<SlotDTO> getSlots() {
        return slots;
    }

    /**
     * @param slots the slots to set
     */
    public void setSlots(Collection<SlotDTO> slots) {
        this.slots = slots;
    }

    /**
     * @return the designations
     */
    public Collection<DefinitionDTO> getDesignations() {
        return designations;
    }

    /**
     * @param designations the designations to set
     */
    public void setDesignations(Collection<DefinitionDTO> designations) {
        this.designations = designations;
    }

}
