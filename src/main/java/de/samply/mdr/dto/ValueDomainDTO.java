/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dto;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * The data transfer object for value domains.
 *
 * The NON_NULL inclusion annotation makes Jackson ignore all NULL fields.
 * Because the ValueDomainDTO combines two value domains (enumerated and described), this
 * annotation is necessary
 *
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValueDomainDTO {

    private String datatype;

    private String format;

    private String unitOfMeasure;

    private String maximumCharacters;

    private String valueDomainType;

    private String validationType;

    private String validationData;

    private Collection<PermissibleValueDTO> permissibleValues;

    private String description;

    private Collection<DefinitionDTO> meanings;

    private Collection<DefinitionDTO> errorMessages;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement(name = "meanings")
    public Collection<DefinitionDTO> getMeanings() {
        return meanings;
    }

    public void setMeanings(Collection<DefinitionDTO> meanings) {
        this.meanings = meanings;
    }


    @XmlElement(name = "value_domain_type")
    public String getValueDomainType() {
        return valueDomainType;
    }

    @XmlElement(name = "permissible_values")
    public Collection<PermissibleValueDTO> getPermissibleValues() {
        return permissibleValues;
    }

    public void setPermissibleValues(Collection<PermissibleValueDTO> permissibleValues) {
        this.permissibleValues = permissibleValues;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    @XmlAttribute(name = "unit_of_measure")
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    @XmlAttribute(name = "maximum_character_quantity")
    public String getMaximumCharacters() {
        return maximumCharacters;
    }

    public void setMaximumCharacters(String maximumCharacters) {
        this.maximumCharacters = maximumCharacters;
    }

    public void setValueDomainType(String valueDomainType) {
        this.valueDomainType = valueDomainType;
    }

    /**
     * @return the validationType
     */
    @XmlElement(name = "validation_type")
    public String getValidationType() {
        return validationType;
    }

    /**
     * @param validationType the validationType to set
     */
    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    /**
     * @return the validationData
     */
    @XmlElement(name = "validation_data")
    public String getValidationData() {
        return validationData;
    }

    /**
     * @param validationData the validationData to set
     */
    public void setValidationData(String validationData) {
        this.validationData = validationData;
    }

    /**
     * @return the errorMessages
     */
    public Collection<DefinitionDTO> getErrorMessages() {
        return errorMessages;
    }

    /**
     * @param errorMessages the errorMessages to set
     */
    public void setErrorMessages(Collection<DefinitionDTO> errorMessages) {
        this.errorMessages = errorMessages;
    }

}
