/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dto;

import javax.xml.bind.annotation.XmlRootElement;

import de.samply.mdr.rest.exceptions.InvalidURNException;

/**
 * The data transfer object for scoped identifiers.
 *
 */
@XmlRootElement
public class ScopedIdentifierDTO {

    /**
     * The namespace of this scoped identifier, e.g. "osse-1" or "dimdi".
     */
    private String namespace;

    /**
     * The type of element this scoped identifier is used for, e.g. "dataelement"
     */
    private String type;

    /**
     * The identifier, e.g. "45" or "LK_45"
     */
    private String identifier;

    /**
     * The revision of this scoped identifier.
     */
    private String version;

    /**
     * Parses the given String into a Scoped identifier.
     * @param urn
     * @return
     */
    public static ScopedIdentifierDTO parseFromURN(String urn) {
        ScopedIdentifierDTO identifier = new ScopedIdentifierDTO();
        String[] parts = urn.split(":");

        if(parts.length != 5 || !parts[0].equals("urn")) {
            throw new InvalidURNException();
        }

        identifier.setNamespace(parts[1]);
        identifier.setType(parts[2]);
        identifier.setIdentifier(parts[3]);
        identifier.setVersion(parts[4]);

        return identifier;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrn() {
        return "urn:" + getNamespace() + ":" + getType() + ":" + getIdentifier() + ":" + getVersion();
    }

    @Override
    public String toString() {
        return getUrn();
    }

}
