/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import de.samply.mdr.rest.exceptions.InvalidAccessTokenException;
import de.samply.mdr.rest.exceptions.InvalidURNException;
import de.samply.mdr.rest.exceptions.MDRRestException;

/**
 * Creates the proper response for MDRRestExceptions.
 *
 */
@Provider
public class ExceptionHandler implements ExceptionMapper<MDRRestException> {

    private static final Logger logger = LogManager.getLogger(ExceptionHandler.class);

    @Override
    public Response toResponse(MDRRestException ex) {
        if(ex instanceof ElementNotFoundException) {
            logger.error("Element not found: " + ((ElementNotFoundException) ex).getElement());
            return Response.status(404).entity(construct404((ElementNotFoundException) ex)).type("text/plain").build();
        }

        if(ex instanceof InvalidURNException) {
            return Response.status(404).entity(ex.getMessage()).type("text/plain").build();
        }

        if(ex instanceof InvalidAccessTokenException) {
            return Response.status(403).entity("Invalid access token").type("text/plain").build();
        }

        logger.error("Exception thrown: ", ex);
        return Response.status(500).type("text/plain").build();
    }

    private String construct404(ElementNotFoundException ex) {
        StringBuilder builder = new StringBuilder();
        builder.append("Unknown element: ");
        builder.append(ex.getElement());
        return builder.toString();
    }

}
