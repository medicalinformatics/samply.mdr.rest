/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.rest;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dto.DataElementDTO;
import de.samply.mdr.dto.DefinitionDTO;
import de.samply.mdr.dto.HierarchyDTO;
import de.samply.mdr.dto.SlotDTO;
import de.samply.mdr.dto.UIModelDTO;
import de.samply.mdr.dto.ValueDomainDTO;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import de.samply.sdao.DAOException;

/**
 * Handles all dataelement requests
 *
 */
@Path("/dataelements")
public class DataElementHandler extends AbstractHandler {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/labels")
    public Collection<DefinitionDTO> getLabels(@PathParam("urn") String urn) throws DAOException {
        try (MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getDefinitions(element);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/validations")
    public ValueDomainDTO getValidations(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getValueDomain(element, mdr);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/catalog")
    public HierarchyDTO getCatalog(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getCatalog(element, mdr);
        }
    }

    @Deprecated
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/uimodel")
    public UIModelDTO getUIModel(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            UIModelDTO model = new UIModelDTO();
            model.setDesignations(getDefinitions(element));
            model.setValueDomain(getValueDomain(element, mdr));
            return model;
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}/slots")
    public Collection<SlotDTO> getSlots(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getSlots(element.getScoped().toString(), mdr);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}")
    public DataElementDTO getDataElement(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getDataElementDTO(element, mdr);
        }
    }

    /*
     * For now, it is not possible to create new data elements with the rest interface
     */
//	@POST
//	@Path("/new/{urn:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public ScopedIdentifierDTO newDataElement(DataElementDTO dto, @PathParam("urn") String urn) throws MDRException {
//
//		try (MDRConnection mdr = new MDRConnection()) {
//			ScopedIdentifierDTO urnDTO = ScopedIdentifierDTO.parseFromURN(urn);
//			Namespace namespace = getNamespace(urnDTO.getNamespace(), mdr);
//			ValueDomainDTO valueDTO = dto.getValueDomain();
//
//			ValueDomain valueDomain = convert(valueDTO);
//
//			if (valueDomain instanceof DescribedValueDomain) {
//				DescribedValueDomain described = (DescribedValueDomain) valueDomain;
//				described.setValidationType(ValidationType.NONE);
//				described.setValidationData("");
//				mdr.getDescribedDao().saveDescribedValueDomain(described);
//			} else {
//				EnumeratedValueDomain enumerated = (EnumeratedValueDomain) valueDomain;
//				mdr.getEnumeratedDao().saveEnumeratedValueDomain(enumerated);
//			}
//
//			DataElement element = new DataElement();
//			element.setValueDomainId(valueDomain.getId());
//			mdr.getDataElementDao().saveDataElement(element);
//
//			for (SlotDTO slotdto : dto.getSlots()) {
//				Slot slot = new Slot();
//				slot.setKey(slotdto.getKey());
//				slot.setValue(slotdto.getValue());
//				slot.setIdentifiedId(element.getIdentifiedId());
//				mdr.getSlotDao().saveSlot(slot);
//			}
//
//			ScopedIdentifier identifier = new ScopedIdentifier();
//			identifier.setStatus(Status.RELEASED);
//			identifier.setNamespaceId(mdr.getNamespaceDao().findNamespaces()
//					.get(0).getId());
//			identifier.setVersion(urnDTO.getVersion());
//			identifier.setIdentifiedId(element.getIdentifiedId());
//			identifier.setNamespaceId(namespace.getId());
//			identifier.setNamespace(namespace.getName());
//			identifier.setIdentifier("" + urnDTO.getIdentifier());
//			identifier.setElementType(ElementType.DATAELEMENT);
//			identifier.setUrl("none");
//			identifier.setCreatedBy(1);
//
//			mdr.getScopedDao().saveScopedIdentifier(identifier);
//
//			for (DefinitionDTO defdes : dto.getDesignations()) {
//				Definition def = new Definition();
//				def.setDefinition(defdes.getDefinition());
//				def.setDesignation(defdes.getDesignation());
//				def.setLanguage(defdes.getLanguage());
//				def.setDesignatableId(element.getDesignatableId());
//				def.setScopedIdentifierId(identifier.getId());
//				mdr.saveDefDesignation(def);
//			}
//
//			if (valueDomain instanceof DescribedValueDomain) {
//				for (DefinitionDTO meaning : valueDTO.getMeanings()) {
//					DescribedValueDomain described = (DescribedValueDomain) valueDomain;
//					Definition def = new Definition();
//					def.setDefinition(meaning.getDefinition());
//					def.setDesignation(meaning.getDesignation());
//					def.setLanguage(meaning.getLanguage());
//					def.setDesignatableId(described.getDesignatableId());
//					def.setScopedIdentifierId(identifier.getId());
//					mdr.saveDefDesignation(def);
//				}
//			} else {
//				for (PermissibleValueDTO permissible : valueDTO
//						.getPermissibleValues()) {
//					PermissibleValue value = new PermissibleValue();
//					value.setPermittedValue(permissible.getValue());
//					value.setEnumeratedValueDomainId(valueDomain.getId());
//					mdr.getPermissibleDao().savePermissibleValue(value);
//
//					for (DefinitionDTO meaning : permissible.getMeanings()) {
//						Definition def = new Definition();
//						def.setDesignatableId(value.getDesignatableId());
//						def.setLanguage(meaning.getLanguage());
//						def.setDefinition(meaning.getDefinition());
//						def.setDesignation(meaning.getDesignation());
//						def.setScopedIdentifierId(identifier.getId());
//						mdr.saveDefDesignation(def);
//					}
//				}
//			}
//
//			mdr.commit();
//
//			ScopedIdentifierDTO scoped = new ScopedIdentifierDTO();
//			scoped.setNamespace(namespace.getName());
//			scoped.setVersion(identifier.getVersion());
//			scoped.setType("dataelement");
//			scoped.setIdentifier(identifier.getIdentifier());
//
//
//			return scoped;
//		}
//	}

}
