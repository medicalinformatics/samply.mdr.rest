/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.dto.DataElementSearchDTO;
import de.samply.mdr.dto.NamespaceDTO;
import de.samply.mdr.dto.ResultDTO;
import de.samply.sdao.DAOException;

/**
 * Handles all namespace requests
 *
 */
@Path("/namespaces")
public class NamespaceHandler extends AbstractHandler {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<NamespaceDTO> getNamespaces() throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            List<DescribedElement> readable = mdr.get(NamespaceDAO.class).getReadableNamespaces();
            List<DescribedElement> writable = mdr.get(NamespaceDAO.class).getWritableNamespaces();
            return convert(readable, writable);
        }
    }

    @Path("/{namespace}/members")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public DataElementSearchDTO getRootElements(@PathParam("namespace") String namespace) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            Collection<ResultDTO> elements = convert(mdr.get(IdentifiedDAO.class).getRootElements(namespace));
            DataElementSearchDTO dto = new DataElementSearchDTO();
            dto.setTotalCount(elements.size());
            dto.setResults(elements);
            return dto;
        }
    }

    @Path("/{namespace}/search")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public DataElementSearchDTO search(@PathParam("namespace") String namespace,
            @QueryParam("query") String query, @QueryParam("type") List<String> types) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            List<ElementType> typeList = new ArrayList<>();
            for(String type : types) {
                typeList.add(ElementType.valueOf(type));
            }
            DataElementSearchDTO dto = new DataElementSearchDTO();
            Collection<ResultDTO> results = convert(mdr.get(IdentifiedDAO.class).findElements(query, namespace,
                    typeList.toArray(new ElementType[types.size()]), Status.values(),
                    new HashMap<String, String>()));
            dto.setResults(results);
            dto.setTotalCount(results.size());
            return dto;
        }
    }

}
