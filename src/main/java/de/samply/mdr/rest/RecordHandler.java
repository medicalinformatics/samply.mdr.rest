/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dto.DefinitionDTO;
import de.samply.mdr.dto.IdentificationDTO;
import de.samply.mdr.dto.RecordEntryDTO;
import de.samply.mdr.dto.SlotDTO;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import de.samply.sdao.DAOException;

/**
 * Handles all record requests.
 *
 */
@Path("/records")
public class RecordHandler extends AbstractHandler {


    /*
     * For now it is not possible to create new records in the rest interface
     */
//	@POST
//	@Path("/new/{urn:(urn:[^:\\/]+?:record:[^\\/]+?)}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public ScopedIdentifierDTO addNewRecord(RecordDTO dto, @PathParam("urn") String urn) throws MDRException {
//		try(MDRConnection mdr = new MDRConnection()) {
//			ScopedIdentifierDTO urnDTO = ScopedIdentifierDTO.parseFromURN(urn);
//			Namespace namespace = mdr.getNamespaceDao().findNamespaceByName(urnDTO.getNamespace());
//
//			Record record = new Record();
//			mdr.getRecordDao().saveRecord(record);
//
//			ScopedIdentifier identifier = new ScopedIdentifier();
//			identifier.setIdentifiedId(record.getIdentifiedId());
//			identifier.setIdentifier(urnDTO.getIdentifier());
//			identifier.setStatus(Status.RELEASED);
//			identifier.setNamespaceId(namespace.getId());
//			identifier.setVersion(urnDTO.getVersion());
//			identifier.setElementType(ElementType.RECORD);
//			identifier.setUrl("none");
//			identifier.setCreatedBy(1);
//
//			mdr.getScopedDao().saveScopedIdentifier(identifier);
//
//			for(DefinitionDTO ddto : dto.getDesignations()) {
//				Definition definition = convert(ddto);
//				definition.setDesignatableId(record.getDesignatableId());
//				definition.setScopedIdentifierId(identifier.getId());
//				mdr.getDefinitionDao().saveDefinition(definition);
//			}
//
//			ScopedIdentifierDTO scopedDTO = new ScopedIdentifierDTO();
//			scopedDTO.setIdentifier(identifier.getIdentifier());
//			scopedDTO.setNamespace(urnDTO.getNamespace());
//			scopedDTO.setType("record");
//			scopedDTO.setVersion(identifier.getVersion());
//
//			mdr.commit();
//
//			return scopedDTO;
//		}
//	}
//
//	@POST
//	@Produces(MediaType.TEXT_PLAIN)
//	@Path("/{urn:(urn:[^:\\/]+?:record:[^\\/]+?)}/addentry/{order}/{dataelement:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}")
//	public Response addEntry(@PathParam("urn") String urn, @PathParam("dataelement") String elementURN,
//			@PathParam("order") int order) throws MDRException {
//		try(MDRConnection mdr = new MDRConnection()) {
//			ScopedIdentifier recordIdentifier = mdr.getScopedDao().getScopedIdentifier(urn);
//			ScopedIdentifier identifier = mdr.getScopedDao().getScopedIdentifier(elementURN);
//
//			if(recordIdentifier == null) {
//				throw new ElementNotFoundException(urn);
//			}
//
//			if(identifier == null) {
//				throw new ElementNotFoundException(elementURN);
//			}
//
//			if(identifier.getElementType() != ElementType.DATAELEMENT) {
//				throw new InvalidArgumentException();
//			}
//
//			mdr.getScopedDao().addSubIdentifier(recordIdentifier.getId(), identifier.getId(), order);
//
//			mdr.commit();
//
//			return Response.status(200).build();
//		}
//	}

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:record:[^\\/]+?)}/labels")
    public Collection<DefinitionDTO> getLabels(@PathParam("urn") String urn) throws DAOException {
        try (MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getDefinitions(element);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:record:[^\\/]+?)}/slots")
    public Collection<SlotDTO> getSlots(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getSlots(element.getScoped().toString(), mdr);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:record:[^\\/]+?)}/members")
    public Collection<RecordEntryDTO> getRecordEntries(@PathParam("urn") String urn) throws DAOException {
        try (MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            Collection<RecordEntryDTO> target = new ArrayList<>();

            List<IdentifiedElement> entries = mdr.get(IdentifiedDAO.class).getEntries(element.getScoped().toString());
            int order = 1;

            for(IdentifiedElement entry : entries) {
                RecordEntryDTO dto = new RecordEntryDTO();
                dto.setOrder(order ++);
                dto.setId(entry.getScoped().toString());
                dto.setDefinitions(getDefinitions(entry));
                dto.setIdentification(new IdentificationDTO(entry.getScoped()));
                target.add(dto);
            }

            return target;
        }
    }

}
