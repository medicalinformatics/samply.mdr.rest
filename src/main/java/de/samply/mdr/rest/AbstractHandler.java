/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.rest;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import de.samply.mdr.dal.dto.CatalogValueDomain;
import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.DescribedValueDomain;
import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.HierarchyNode;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dal.dto.ValidationType;
import de.samply.mdr.dal.dto.ValueDomain;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.SlotDAO;
import de.samply.mdr.dto.CodeDTO;
import de.samply.mdr.dto.DataElementDTO;
import de.samply.mdr.dto.DefinitionDTO;
import de.samply.mdr.dto.HierarchyDTO;
import de.samply.mdr.dto.IdentificationDTO;
import de.samply.mdr.dto.NamespaceDTO;
import de.samply.mdr.dto.NodeDTO;
import de.samply.mdr.dto.PermissibleValueDTO;
import de.samply.mdr.dto.ResultDTO;
import de.samply.mdr.dto.ScopedIdentifierDTO;
import de.samply.mdr.dto.SlotDTO;
import de.samply.mdr.dto.ValueDomainDTO;
import de.samply.mdr.rest.AbstractHandler.Tree.Node;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import de.samply.sdao.DAOException;

/**
 * This abstract handler creates data transfer objects using
 * the POJOs from the database access layer.
 *
 */
public class AbstractHandler {

    @Inject
    protected User user;

    @Context
    protected ServletContext context;

    @Context
    protected HttpHeaders headers;

    protected String rangeRegex = "(?:(.+)<=)?x(?:<=(.+))?";

    /**
     * Creates a DTO for an element.
     * @param element
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected DataElementDTO getDataElementDTO(IdentifiedElement element, MDRConnection mdr) throws DAOException {
        DataElementDTO dto = new DataElementDTO();
        dto.setSlots(getSlots(element.getScoped().toString(), mdr));
        dto.setDesignations(getDefinitions(element));
        dto.setValueDomain(getValueDomain(element, mdr));
        dto.setIdentification(new IdentificationDTO(element.getScoped()));
        return dto;
    }

    /**
     * Creates all slot DTOs for an URN
     * @param urn
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected Collection<SlotDTO> getSlots(String urn, MDRConnection mdr) throws DAOException {
        return SlotDTO.convert(mdr.get(SlotDAO.class).getSlots(urn));
    }

    /**
     * Creates a value domain DTO for an element.
     * @param element
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected ValueDomainDTO getValueDomain(IdentifiedElement element, MDRConnection mdr) throws DAOException {
        DataElement de = (DataElement) element.getElement();
        List<String> locales = Util.getLocale(headers);

        if(de == null) {
            throw new ElementNotFoundException(element.getScoped().toString());
        } else {
            ValueDomain domain = (ValueDomain) mdr.get(ElementDAO.class).getElement(de.getValueDomainId());
            if(domain instanceof DescribedValueDomain) {
                return convert((DescribedValueDomain) domain, element.getScoped().getId(), mdr, locales);
            } else if(domain instanceof EnumeratedValueDomain) {
                return convert((EnumeratedValueDomain) domain, element.getScoped().getId(), mdr);
            } else if(domain instanceof CatalogValueDomain) {
                /**
                 * As a workaround just handle the catalog value domain as an enumerated value domain.
                 */
                return convert((CatalogValueDomain) domain, element.getScoped().getId(), mdr);
            } else {
                throw new ElementNotFoundException(element.getScoped().toString());
            }
        }
    }


    /**
     * Creates definition DTOs for an element.
     * @param element
     * @return
     */
    protected Collection<DefinitionDTO> getDefinitions(IdentifiedElement element) {
        return getDefinitions(element.getDefinitions());
    }

    /**
     * Creates definition DTOs from the specified definitions, using the locale headers.
     * Returns always a list with at least one item.
     * @param definitions
     * @return
     */
    protected Collection<DefinitionDTO> getDefinitions(Collection<Definition> definitions) {
        List<String> locales = Util.getLocale(headers);

        if(locales.size() == 0) {
            return convert(definitions);
        } else {
            Collection<DefinitionDTO> target = new ArrayList<>();

            for(Definition def : definitions) {
                if(locales.contains(def.getLanguage())) {
                    target.add(convert(def));
                }
            }

            if(target.size() == 0 && definitions.size() > 0) {
                target.add(convert(definitions.iterator().next()));
            }

            return target;
        }
    }

    /**
     * Creates a definition POJO from a dto. Was used in the methods, that create
     * new dataelements.
     * @param dto
     * @return
     */
    protected Definition convert(DefinitionDTO dto) {
        Definition d = new Definition();
        d.setDesignation(dto.getDesignation());
        d.setDefinition(dto.getDefinition());
        d.setLanguage(dto.getLanguage());
        return d;
    }

    /**
     * Searches the MDR for the specified namespace and returns it.
     * If the namespace can not be found, this method returns an ElementNotFoundException
     * @param name
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected Namespace getNamespace(String name, MDRConnection mdr) throws DAOException {
        Namespace namespace = mdr.get(ElementDAO.class).getNamespace(name);
        if(namespace == null) {
            throw new ElementNotFoundException("Namespace " + name);
        } else {
            return namespace;
        }
    }

    /**
     * Returns the namespace for the specified identifier.
     * @param identifier
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected Namespace getNamespace(ScopedIdentifierDTO identifier,
            MDRConnection mdr) throws DAOException {
        return getNamespace(identifier.getNamespace(), mdr);
    }

    /**
     * Creates a permissible value DTO from the permissible value POJO for the specified scopedIdentifier ID.
     * @param v
     * @param scopedIdentifierId
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected PermissibleValueDTO convert(PermissibleValue v, int scopedIdentifierId, MDRConnection mdr) throws DAOException {
        PermissibleValueDTO dto = new PermissibleValueDTO();

        DefinitionDAO dao = mdr.get(DefinitionDAO.class);

        dto.setValue(v.getPermittedValue());
        dto.setMeanings(new ArrayList<DefinitionDTO>());

        dto.setMeanings(getDefinitions(dao.getDefinitions(v.getId(), scopedIdentifierId)));

        return dto;
    }

    /**
     * Creates a ValueDomainDTO from an enumerated value domain POJO for the specified scoped identifier ID.
     * @param en
     * @param scopedIdentifierId
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected ValueDomainDTO convert(EnumeratedValueDomain en, int scopedIdentifierId, MDRConnection mdr) throws DAOException {
        ValueDomainDTO dto = new ValueDomainDTO();

        dto.setDatatype(en.getDatatype());
        dto.setFormat(en.getFormat());
        dto.setMaximumCharacters("" + en.getMaxCharacters());
        dto.setUnitOfMeasure(en.getUnitOfMeasure());

        dto.setPermissibleValues(new ArrayList<PermissibleValueDTO>());
        for(Element v : mdr.get(ElementDAO.class).getPermissibleValues(en.getId())) {
            dto.getPermissibleValues().add(convert((PermissibleValue) v, scopedIdentifierId, mdr));
        }

        dto.setValueDomainType(STRING_ENUMERATED_VALUE_DOMAIN);

        return dto;
    }

    /**
     * Creates a new value domain DTO for the given catalog value domain.
     * @param domain
     * @param id
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected ValueDomainDTO convert(CatalogValueDomain domain, int id, MDRConnection mdr) throws DAOException {
        ValueDomainDTO dto = new ValueDomainDTO();

        dto.setDatatype(STRING_CATALOG_VALUE_DOMAIN_DATATYPE);
        dto.setFormat(STRING_CATALOG_VALUE_DOMAIN);
        dto.setValueDomainType(STRING_CATALOG_VALUE_DOMAIN);

        dto.setUnitOfMeasure(null);
        return dto;
    }

    /**
     * Converts the given code into a PermissibleValue. Only used in the v2 REST interface.
     * @param element
     * @return
     */
    protected PermissibleValueDTO convertCode(IdentifiedElement element) {
        PermissibleValueDTO dto = new PermissibleValueDTO();
        Code code = (Code) element.getElement();
        dto.setValue(code.getCode());
        dto.setMeanings(getDefinitions(element));
        return dto;
    }

    /**
     * Returns the Catalog in the context of the given dateelement. The output may be filtered (removed
     * empty branches of the hierarchy).
     * @param element
     * @param mdr
     * @return
     * @throws DAOException
     */
    protected HierarchyDTO getCatalog(IdentifiedElement element, MDRConnection mdr) throws DAOException {
        HierarchyDTO dto = new HierarchyDTO();
        DataElement dataelement = (DataElement) element.getElement();

        Element valueDomain = mdr.get(ElementDAO.class).getElement(dataelement.getValueDomainId());

        if(!(valueDomain instanceof CatalogValueDomain)) {
            throw new BadRequestException();
        }

        CatalogValueDomain catalogValueDomain = (CatalogValueDomain) valueDomain;

        /**
         * The catalog, the list of permissible codes in this catalog value domain,
         * the hierarchy nodes for this catalog and the list of all codes in this catalog.
         */
        IdentifiedElement catalog = mdr.get(IdentifiedDAO.class).getElement(catalogValueDomain.getCatalogScopedIdentifierId());
        List<IdentifiedElement> permissibleCodes = mdr.get(IdentifiedDAO.class).getPermissibleCodes(catalogValueDomain);
        List<HierarchyNode> nodes = mdr.getHierarchyNodes(catalog.getScoped().toString());
        List<IdentifiedElement> codes = mdr.get(IdentifiedDAO.class).getAllSubMembers(catalog.getScoped().toString());

        Map<Integer, IdentifiedElement> permissibleCodeMap = new HashMap<>();
        Map<Integer, Node<CodeDTO>> nodeMap = new HashMap<>();

        Tree<CodeDTO> tree = new Tree<CodeDTO>(new CodeDTO());
        tree.root.data.setIsValid(false);

        nodeMap.put(catalog.getScoped().getId(), tree.root);

        /**
         * Put all permissible codes into a separate map
         */
        for(IdentifiedElement code : permissibleCodes) {
            permissibleCodeMap.put(code.getScoped().getId(), code);
        }

        /**
         * If a code from the catalog is not in the map of permissible codes,
         * it is not a valid in the context of the dataelement. For each code
         * create a new node and put it in the map.
         */
        for(IdentifiedElement code : codes) {
            if(!permissibleCodeMap.containsKey(code.getScoped().getId())) {
                ((Code)code.getElement()).setValid(false);
            }

            CodeDTO codeDTO = new CodeDTO();
            Code c = (Code) code.getElement();
            codeDTO.setDesignations(getDefinitions(code));
            codeDTO.setIdentification(new IdentificationDTO(code.getScoped()));
            codeDTO.setIsValid(c.isValid());
            codeDTO.setCode(c.getCode());
            nodeMap.put(code.getScoped().getId(), new Node<CodeDTO>(codeDTO));
        }

        /**
         * Build the tree using the hierarchy nodes and the nodeMap
         */
        for(HierarchyNode node : nodes) {
            nodeMap.get(node.getSuperId()).children.add(nodeMap.get(node.getSubId()));
        }

        /**
         * Removes invalid codes and filters the tree, meaning it removes "empty" branches with no valid codes and
         */
        filter(tree.root);

        /**
         * Reduce the tree to the node, that represents a valid code or has more than
         * one child.
         */
        reduce(tree);

        List<CodeDTO> codeList = new ArrayList<>();

        /**
         * Create a list for the REST interface using the current tree.
         */
        toList(codeList, tree.root.children);
        dto.setCodes(codeList);

        dto.setRoot(new NodeDTO());
        dto.getRoot().setDesignations(getDefinitions(catalog));
        dto.getRoot().setIdentification(new IdentificationDTO(catalog.getScoped().toString()));
        dto.getRoot().setSubCodes(toCodeList(tree.root.children));
        return dto;
    }

    /**
     *
     * @param codeList
     * @param children
     */
    private void toList(List<CodeDTO> codeList, List<Node<CodeDTO>> children) {
        for(Node<CodeDTO> node : children) {
            node.data.setSubCodes(toCodeList(node.children));
            codeList.add(node.data);

            toList(codeList, node.children);
        }
    }

    /**
     * Returns a List of all sub codes as strings.
     * @param children
     * @return
     */
    private List<String> toCodeList(List<Node<CodeDTO>> children) {
        List<String> target = new ArrayList<>();
        for(Node<CodeDTO> code : children) {
            target.add(code.data.getCode());
        }
        return target;
    }

    /**
     * Reduces the given tree.
     * @param tree
     */
    private void reduce(Tree<CodeDTO> tree) {
        tree.root = reduce(tree.root);
    }

    /**
     * Reduces the given node and returns the new node.
     * @param node
     * @return
     */
    private Node<CodeDTO> reduce(Node<CodeDTO> node) {
        if(node.data.getIsValid()) {
            return node;
        }

        if(node.children.size() > 1) {
            return node;
        }

        if(node.children.size() == 1) {
            return reduce(node.children.get(0));
        }

        return null;
    }

    /**
     * Removed invalid codes without children.
     * @param node
     */
    private void filter(Node<CodeDTO> node) {
        Iterator<Node<CodeDTO>> iterator = node.children.iterator();
        while(iterator.hasNext()) {
            Node<CodeDTO> next = iterator.next();

            filter(next);

            if(!next.data.getIsValid() && next.children.size() == 0) {
                iterator.remove();
            }
        }
    }

    /**
     * Creates a value domain POJO for the specified DTO. Was used in the methods, that create
     * new dataelements.
     * @param valueDTO
     * @return
     */
    protected ValueDomain convert(ValueDomainDTO valueDTO) {
        ValueDomain domain = null;
        if(valueDTO.getValueDomainType().equals(STRING_DESCRIBED_VALUE_DOMAIN)) {
            DescribedValueDomain described = new DescribedValueDomain();
            described.setDescription(valueDTO.getDescription());
            domain = described;
        } else {
            domain = new EnumeratedValueDomain();
        }

        domain.setDatatype(valueDTO.getDatatype());
        domain.setFormat(valueDTO.getFormat());
        domain.setMaxCharacters(Integer.parseInt(valueDTO.getMaximumCharacters()));
        domain.setUnitOfMeasure(valueDTO.getUnitOfMeasure());

        return domain;
    }

    /**
     * Creates a value domain DTO from the specified described value domain POJO for the specified
     * scoped identifier ID and locales.
     * @param va
     * @param scopedIdentifierId
     * @param mdr
     * @param locales
     * @return
     * @throws DAOException
     */
    protected ValueDomainDTO convert(DescribedValueDomain va, int scopedIdentifierId, MDRConnection mdr, List<String> locales) throws DAOException {
        ValueDomainDTO dto = new ValueDomainDTO();

        dto.setDatatype(va.getDatatype());
        dto.setDescription(va.getDescription());
        dto.setFormat(va.getFormat());
        dto.setMaximumCharacters("" + va.getMaxCharacters());
        dto.setUnitOfMeasure(va.getUnitOfMeasure());
        dto.setValidationType(va.getValidationType().toString());
        dto.setValidationData(va.getValidationData());
        dto.setErrorMessages(getErrorMessages(va, locales));

        DefinitionDAO dao = mdr.get(DefinitionDAO.class);

        dto.setMeanings(getDefinitions(dao.getDefinitions(va.getId(), scopedIdentifierId)));
        dto.setValueDomainType(STRING_DESCRIBED_VALUE_DOMAIN);

        return dto;
    }

    /**
     * Returns a list of error messages for the specified described value domain and locales.
     * @param va
     * @param locales
     * @return
     */
    private Collection<DefinitionDTO> getErrorMessages(DescribedValueDomain va, Collection<String> locales) {
        Collection<DefinitionDTO> target = new ArrayList<>();

        if(va.getValidationType() == ValidationType.NONE) {
            return Collections.emptyList();
        }

        for(String locale : locales) {
            ResourceBundle bundle = ResourceBundle.getBundle("bundles/Messages", new Locale(locale));
            if(bundle != null && bundle.getLocale().getLanguage().equals(locale)) {
                DefinitionDTO dto = getErrorMessage(bundle, va, locale);
                dto.setLanguage(bundle.getLocale().getLanguage());
                target.add(dto);
            }
        }

        if(target.size() == 0) {
            ResourceBundle bundle = ResourceBundle.getBundle("bundles/Messages", new Locale("en"));
            DefinitionDTO dto = getErrorMessage(bundle, va, "en");
            dto.setLanguage(bundle.getLocale().getLanguage());
            target.add(dto);
        }

        return target;
    }

    /**
     * Creates an error message from the specified bundle for the specified value domain and locale.
     * @param bundle
     * @param va
     * @param locale
     * @return
     */
    private DefinitionDTO getErrorMessage(ResourceBundle bundle,
            DescribedValueDomain va, String locale) {
        DefinitionDTO dto = new DefinitionDTO();
        switch(va.getValidationType()) {
        case BOOLEAN:
            dto.setDefinition(bundle.getString("boolean_definition"));
            dto.setDefinition(bundle.getString("boolean_designation"));
            break;

        case INTEGER:
            dto.setDefinition(bundle.getString("integer_definition"));
            dto.setDesignation(bundle.getString("integer_designation"));
            break;

        case FLOAT:
            dto.setDefinition(bundle.getString("float_definition"));
            dto.setDesignation(bundle.getString("float_designation"));
            break;

        case NONE:
            dto.setDefinition(bundle.getString("none_definition"));
            dto.setDesignation(bundle.getString("none_designation"));
            break;

        case REGEX:
            dto.setDefinition(MessageFormat.format(bundle.getString("regex_definition"), va.getValidationData()));
            dto.setDesignation(bundle.getString("regex_designation"));
            break;

        case DATE:
            dto.setDefinition(bundle.getString("date_definition"));
            dto.setDesignation(bundle.getString("date_designation"));
            break;

        case TIME:
            dto.setDefinition(bundle.getString("time_definition"));
            dto.setDesignation(bundle.getString("time_designation"));
            break;

        case DATETIME:
            dto.setDefinition(bundle.getString("datetime_definition"));
            dto.setDesignation(bundle.getString("datetime_designation"));
            break;

        case FLOATRANGE:
        case INTEGERRANGE:
            Pattern pattern = Pattern.compile(rangeRegex);
            Matcher matcher = pattern.matcher(va.getValidationData());
            if(matcher.find()) {
                String first = matcher.group(1);
                String second = matcher.group(2);

                if(first != null && second != null) {
                    dto.setDefinition(MessageFormat.format(bundle.getString("range_both_limits_definition"), first, second));
                    dto.setDesignation(bundle.getString("range_invalid"));
                } else if(first == null && second != null) {
                    dto.setDefinition(MessageFormat.format(bundle.getString("range_upper_limit_definition"), second));
                    dto.setDesignation(bundle.getString("range_invalid"));
                } else if(first != null && second == null) {
                    dto.setDefinition(MessageFormat.format(bundle.getString("range_lower_limit_definition"), first));
                    dto.setDesignation(bundle.getString("range_invalid"));
                } else {
                    if(va.getValidationType() == ValidationType.FLOATRANGE) {
                        dto.setDefinition(bundle.getString("float_definition"));
                        dto.setDesignation(bundle.getString("float_designation"));
                    } else {
                        dto.setDefinition(bundle.getString("integer_definition"));
                        dto.setDesignation(bundle.getString("integer_designation"));
                    }
                }
            } else {
                // At this point something has gone horribly wrong (data corruption in the database).
                // It does not make sense to give an invalid error message.
            }
            break;

        case JS:
            break;
        case LUA:
            break;
        default:
            break;

        }
        return dto;
    }

    /**
     * Creates a list of namespace DTOs from the specified readable namespace list and
     * writable namespace list.
     * @param readable
     * @param writable
     * @return
     */
    protected Collection<NamespaceDTO> convert(List<DescribedElement> readable,
            List<DescribedElement> writable) {
        HashSet<String> writableHash = new HashSet<>();

        for(DescribedElement ns : writable) {
            if(ns.getElement() instanceof Namespace) {
                Namespace n = (Namespace) ns.getElement();
                writableHash.add(n.getName());
            }
        }

        List<NamespaceDTO> target = new ArrayList<>();

        for(DescribedElement ns : readable) {
            if(ns.getElement() instanceof Namespace) {
                Namespace n = (Namespace) ns.getElement();
                NamespaceDTO dto = new NamespaceDTO();
                dto.setName(n.getName());
                dto.setDefinitions(getDefinitions(ns.getDefinitions()));
                dto.setWritable(writableHash.contains(n.getName()));
                target.add(dto);
            }
        }

        return target;
    }

    protected Collection<ResultDTO> convert(List<IdentifiedElement> elements) {
        List<ResultDTO> target = new ArrayList<ResultDTO>();

        for(IdentifiedElement element : elements) {
            ElementType elementType = element.getElementType();

            if(!filter(elementType)) {
                ResultDTO record = new ResultDTO();
                record.setId(element.getScoped().toString());
                record.setDefinitions(getDefinitions(element));
                record.setIdentification(new IdentificationDTO(element.getScoped()));
                target.add(record);
            }
        }
        return target;
    }

    protected boolean filter(ElementType type) {
        switch(type) {
            case DATAELEMENT:
            case DATAELEMENTGROUP:
            case RECORD:
                return false;

            default:
                return true;
        }
    }

    protected static class Tree<T> {
        private Node<T> root;

        public Tree(T rootData) {
            root = new Node<T>(null);
            root.data = rootData;
            root.children = new ArrayList<Node<T>>();
        }

        public static class Node<T> {
            private T data;
            private List<Node<T>> children = new ArrayList<>();

            public Node(T data) {
                this.data = data;
            }
        }
    }

    public static DefinitionDTO convert(Definition d) {
        DefinitionDTO dto = new DefinitionDTO();
        dto.setLanguage(d.getLanguage());
        dto.setDesignation(d.getDesignation());
        dto.setDefinition(d.getDefinition());
        return dto;
    }

    public static Collection<DefinitionDTO> convert(Collection<Definition> c) {
        List<DefinitionDTO> target = new ArrayList<DefinitionDTO>();
        for(Definition d : c) {
            target.add(convert(d));
        }
        return target;
    }

    public static final String STRING_DESCRIBED_VALUE_DOMAIN = "described";
    public static final String STRING_ENUMERATED_VALUE_DOMAIN = "enumerated";
    public static final String STRING_CATALOG_VALUE_DOMAIN = "catalog";
    public static final String STRING_CATALOG_VALUE_DOMAIN_DATATYPE = "CATALOG";

}
