/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dto.DataElementGroupDTO;
import de.samply.mdr.dto.DataElementSearchDTO;
import de.samply.mdr.dto.DefinitionDTO;
import de.samply.mdr.dto.IdentificationDTO;
import de.samply.mdr.dto.ResultDTO;
import de.samply.mdr.dto.SlotDTO;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import de.samply.sdao.DAOException;

/**
 * Handles all dataelementgroup requests
 *
 */
@Path("/dataelementgroups")
public class DataElementGroupHandler extends AbstractHandler {

    /*
     * For now, it is not possible to create new dataelementgroups with the rest interface
     */
//	@POST
//	@Path("/new/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public ScopedIdentifierDTO addNewGroup(DataElementGroupDTO dto, @PathParam("urn") String urn)
//			throws MDRException {
//		try(MDRConnection mdr = new MDRConnection()) {
//			ScopedIdentifierDTO urnDTO = ScopedIdentifierDTO.parseFromURN(urn);
//			Namespace namespace = mdr.getNamespaceDao().findNamespaceByName(urnDTO.getNamespace());
//
//			DataElementGroup group = new DataElementGroup();
//
//			mdr.getDataElementGroupDao().saveDataElementGroup(group);
//
//			ScopedIdentifier identifier = new ScopedIdentifier();
//			identifier.setIdentifiedId(group.getIdentifiedId());
//			identifier.setIdentifier(urnDTO.getIdentifier());
//			identifier.setStatus(Status.RELEASED);
//			identifier.setNamespaceId(namespace.getId());
//			identifier.setNamespace(namespace.getName());
//			identifier.setElementType(ElementType.DATAELEMENTGROUP);
//			identifier.setVersion(urnDTO.getVersion());
//			identifier.setUrl("none");
//			identifier.setCreatedBy(1);
//
//			mdr.getScopedDao().saveScopedIdentifier(identifier);
//
//			for(DefinitionDTO def : dto.getDesignations()) {
//				Definition definition = convert(def);
//				definition.setDesignatableId(group.getDesignatableId());
//				definition.setScopedIdentifierId(identifier.getId());
//				mdr.getDefinitionDao().saveDefinition(definition);
//			}
//
//			ScopedIdentifierDTO scopedDTO = new ScopedIdentifierDTO();
//			scopedDTO.setIdentifier(identifier.getIdentifier());
//			scopedDTO.setNamespace(namespace.getName());
//			scopedDTO.setType("dataelementgroup");
//			scopedDTO.setVersion(identifier.getVersion());
//
//			mdr.commit();
//
//			return scopedDTO;
//		}
//	}
//
//	@POST
//	@Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/addmember/{dataelement:(urn:[^:\\/]+?:dataelement:[^\\/]+?)}")
//	@Produces(MediaType.TEXT_PLAIN)
//	public Response addNewMember(@PathParam("urn") String groupURN, @PathParam("dataelement") String elementURN) throws MDRException {
//		try(MDRConnection mdr = new MDRConnection()) {
//
//			DataElementGroup group = mdr.getDataElementGroupDao().getDataElementGroupByURN(groupURN);
//			DataElement element = mdr.getDataElementDao().getDataElementByURN(elementURN);
//
//			if(group == null) {
//				throw new ElementNotFoundException(groupURN);
//			}
//
//			if(element == null) {
//				throw new ElementNotFoundException(groupURN);
//			}
//
//			ScopedIdentifier groupIdentifier = mdr.getScopedDao().getScopedIdentifier(groupURN);
//			ScopedIdentifier elementIdentifier = mdr.getScopedDao().getScopedIdentifier(elementURN);
//
//			mdr.getScopedDao().addSubIdentifier(groupIdentifier.getId(), elementIdentifier.getId());
//
//			mdr.commit();
//			return Response.status(200).build();
//		}
//	}
//
//	@POST
//	@Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/addsubgroup/{suburn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}")
//	@Produces(MediaType.TEXT_PLAIN)
//	public Response addNewSubgroup(@PathParam("urn") String groupURN, @PathParam("suburn") String subGroupURN) throws MDRException {
//		try(MDRConnection mdr = new MDRConnection()) {
//
//			DataElementGroup group = mdr.getDataElementGroupDao().getDataElementGroupByURN(groupURN);
//			DataElementGroup subGroup = mdr.getDataElementGroupDao().getDataElementGroupByURN(subGroupURN);
//
//			if(group == null) {
//				throw new ElementNotFoundException(groupURN);
//			}
//
//			if(subGroup == null) {
//				throw new ElementNotFoundException(subGroupURN);
//			}
//
//			ScopedIdentifier groupIdentifier = mdr.getScopedDao().getScopedIdentifier(groupURN);
//			ScopedIdentifier subgroupIdentifier = mdr.getScopedDao().getScopedIdentifier(subGroupURN);
//
//			mdr.getScopedDao().addSubIdentifier(groupIdentifier.getId(), subgroupIdentifier.getId());
//
//			mdr.commit();
//			return Response.status(200).build();
//		}
//	}

    @GET
    @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/members")
    @Produces(MediaType.APPLICATION_JSON)
    public DataElementSearchDTO getMembers(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }

            DataElementSearchDTO dto = new DataElementSearchDTO();
            List<ResultDTO> records = new ArrayList<ResultDTO>();

            // Because the URN may contain the revision "latest", replace it by the actual URN.
            for(IdentifiedElement member : mdr.get(IdentifiedDAO.class).getSubMembers(element.getScoped().toString())) {
                if(member.getScoped().getStatus() == Status.DRAFT || filter(member.getElementType())) {
                    continue;
                }

                ResultDTO record = new ResultDTO();
                record.setId(member.getScoped().toString());
                record.setDefinitions(getDefinitions(member));
                record.setIdentification(new IdentificationDTO(member.getScoped()));
                records.add(record);
            }

            dto.setTotalCount(records.size());
            dto.setResults(records);

            return dto;
        }
    }

    @GET
    @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/subgroups")
    @Produces(MediaType.APPLICATION_JSON)
    public DataElementSearchDTO getSubgroups(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }

            DataElementSearchDTO dto = new DataElementSearchDTO();
            List<ResultDTO> records = new ArrayList<ResultDTO>();

            // Because the URN may contain the revision "latest", replace it by the actual URN.
            for(IdentifiedElement member : mdr.get(IdentifiedDAO.class).getSubMembers(element.getScoped().toString())) {
                if(member.getElementType() != ElementType.DATAELEMENTGROUP) {
                    continue;
                }

                if(member.getScoped().getStatus() == Status.DRAFT || filter(member.getElementType())) {
                    continue;
                }

                ResultDTO record = new ResultDTO();
                record.setId(member.getScoped().toString());
                record.setDefinitions(getDefinitions(member));
                record.setIdentification(new IdentificationDTO(member.getScoped()));
                records.add(record);
            }

            dto.setTotalCount(records.size());
            dto.setResults(records);

            return dto;
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/slots")
    public Collection<SlotDTO> getSlots(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getSlots(element.getScoped().toString(), mdr);
        }
    }

    @GET
    @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}/labels")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<DefinitionDTO> getDefinitions(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            return getDefinitions(element);
        }
    }

    @GET
    @Path("/{urn:(urn:[^:\\/]+?:dataelementgroup:[^\\/]+?)}")
    @Produces(MediaType.APPLICATION_JSON)
    public DataElementGroupDTO getGroup(@PathParam("urn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }
            DataElementGroupDTO dto = new DataElementGroupDTO();
            dto.setIdentification(new IdentificationDTO(element.getScoped()));
            dto.setDesignations(getDefinitions(element));

            DataElementSearchDTO members = new DataElementSearchDTO();
            List<ResultDTO> records = new ArrayList<ResultDTO>();

            // Because the URN may contain the revision "latest", replace it by the actual URN.
            for(IdentifiedElement member : mdr.get(IdentifiedDAO.class).getSubMembers(element.getScoped().toString())) {
                if(member.getScoped().getStatus() == Status.DRAFT || filter(member.getElementType())) {
                    continue;
                }

                ResultDTO record = new ResultDTO();
                record.setId(member.getScoped().toString());
                record.setDefinitions(getDefinitions(member));
                record.setIdentification(new IdentificationDTO(member.getScoped()));
                records.add(record);
            }

            members.setTotalCount(records.size());
            members.setResults(records);

            dto.setMembers(members);
            dto.setSlots(getSlots(element.getScoped().toString(), mdr));

            return dto;
        }
    }
}
