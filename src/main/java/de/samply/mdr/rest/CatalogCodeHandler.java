/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.rest;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.samply.mdr.dal.dto.Code;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dto.CodeDTO;
import de.samply.mdr.dto.DefinitionDTO;
import de.samply.mdr.dto.IdentificationDTO;
import de.samply.mdr.dto.SlotDTO;
import de.samply.mdr.rest.exceptions.ElementNotFoundException;
import de.samply.sdao.DAOException;

/**
 * Handles all requests for codes inside a catalog.
 */
@Path("/catalogs")
public class CatalogCodeHandler extends AbstractHandler {


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:catalog:[^\\/]+?)}/codes/{codeUrn:(urn:[^:\\/]+?:code:[^\\/]+?)}")
    public CodeDTO getCode(@PathParam("codeUrn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }

            Code code = (Code) element.getElement();

            CodeDTO dto = new CodeDTO();
            dto.setDesignations(getDefinitions(element));
            dto.setSlots(getSlots(element.getScoped().toString(), mdr));
            dto.setIdentification(new IdentificationDTO(element.getScoped()));
            dto.setCode(code.getCode());
            dto.setIsValid(code.isValid());

            return dto;
        }
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:catalog:[^\\/]+?)}/codes/{codeUrn:(urn:[^:\\/]+?:code:[^\\/]+?)}/slots")
    public Collection<SlotDTO> getCodeSlots(@PathParam("codeUrn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }

            return getSlots(element.getScoped().toString(), mdr);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{urn:(urn:[^:\\/]+?:catalog:[^\\/]+?)}/codes/{codeUrn:(urn:[^:\\/]+?:code:[^\\/]+?)}/labels")
    public Collection<DefinitionDTO> getCodeLabels(@PathParam("codeUrn") String urn) throws DAOException {
        try(MDRConnection mdr = ConnectionFactory.get(user.getId())) {
            IdentifiedElement element = mdr.get(IdentifiedDAO.class).getElement(urn);
            if(element == null) {
                throw new ElementNotFoundException(urn);
            }

            return getDefinitions(element);
        }
    }

}
