/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.rest;

import java.io.FileNotFoundException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.xml.sax.SAXException;

import de.samply.config.util.FileFinderUtil;
import de.samply.sdao.DAOException;

/**
 * Initializes the servlet: loads drivers, loads configuration files.
 *
 */
@WebListener
public class ServletListener implements ServletContextListener {

    private static final Logger logger = LogManager.getLogger(MDRRestApplication.class);

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                logger.info("Unregistering driver " + driver.toString());
            } catch (SQLException e) {
            }
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        try {
            String projectName = event.getServletContext().getInitParameter("projectName");

            if(projectName == null) {
                projectName = "samply";
            }

            logger.info("Project name: " + projectName);

            MDRConfig.setProjectName(projectName);

            logger.info("Registering PostgreSQL driver");
            Class.forName("org.postgresql.Driver").newInstance();

            ServletContext context = event.getServletContext();
            String fallback = context.getRealPath("/WEB-INF");
            Configurator.initialize("mdr-rest", FileFinderUtil.findFile("log4j2.xml", projectName, fallback).getAbsolutePath());
            MDRConfig.initialize(fallback);
        } catch (FileNotFoundException | JAXBException | SAXException | ParserConfigurationException | DAOException
                | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            logger.error("Config file not found...", e);
        }
    }

}
